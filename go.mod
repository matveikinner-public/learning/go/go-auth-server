module gitlab.com/matveikinner-public/learning/go/go-auth-server

go 1.18

require github.com/gofiber/fiber/v2 v2.34.0

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/go-xorm/xorm v0.7.9 // indirect
	github.com/klauspost/compress v1.15.0 // indirect
	github.com/lib/pq v1.10.6 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.37.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	xorm.io/builder v0.3.6 // indirect
	xorm.io/core v0.7.2-0.20190928055935-90aeac8d08eb // indirect
)
