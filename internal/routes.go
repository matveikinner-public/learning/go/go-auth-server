package main

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/matveikinner-public/learning/go/go-auth-server/internal/auth"
)

func registerRoutes(app *fiber.App) {

	api := app.Group("/api")

	auth.RegisterAuthControllers(api)
}
