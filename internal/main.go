package main

import (
	"log"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"

	"gitlab.com/matveikinner-public/learning/go/go-auth-server/internal/config/database"
)

func main() {
	// Initialize database
	err := database.InitEngine()
	if err != nil {
		// Panic to stop the execution process
		panic(err)
	}
	// Initialize new Fiber application
	app := fiber.New()

	// Initialize Fiber application logger
	loggerConfig := logger.Config{
		Next:         nil,
		Format:       "[${time}] ${status} - ${latency} ${method} ${path}\n",
		TimeFormat:   "15:04:05",
		TimeZone:     "Local",
		TimeInterval: 500 * time.Millisecond,
		Output:       os.Stderr,
	}

	app.Use(logger.New(loggerConfig))

	registerRoutes(app)

	log.Fatal(app.Listen(":3000"))
}
