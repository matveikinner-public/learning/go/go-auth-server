package database

import (
	"fmt"

	"github.com/go-xorm/xorm"
	_ "github.com/lib/pq"

	"gitlab.com/matveikinner-public/learning/go/go-auth-server/internal/user/models"
)

var DBEngine *xorm.Engine

func InitEngine() error {
	dbHost := "127.0.0.1"
	dbPort := 5432
	dbUser := "postgres"
	dbPass := "secret"
	dbName := "postgres"

	dataSourceName := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPass, dbName)

	// TODO: Is it possible to assign the variable here?
	engine, err := xorm.NewEngine("postgres", dataSourceName)
	if err != nil {
		return err
	}

	DBEngine = engine

	// Ping server to see if its alive
	if err := engine.Ping(); err != nil {
		return err
	}

	// Sync the database
	if err := engine.Sync(new(models.User)); err != nil {
		return err
	}

	return nil
}
