package auth

import (
	"github.com/gofiber/fiber/v2"
	"golang.org/x/crypto/bcrypt"

	auth "gitlab.com/matveikinner-public/learning/go/go-auth-server/internal/auth/models"
	"gitlab.com/matveikinner-public/learning/go/go-auth-server/internal/config/database"
	user "gitlab.com/matveikinner-public/learning/go/go-auth-server/internal/user/models"
)

func RegisterHandler(c *fiber.Ctx) error {
	req := new(auth.AuthCredentials)

	if err := c.BodyParser(req); err != nil {
		return err
	}

	// Hash user password before saving it to the database
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	user := &user.User{
		Email:    req.Email,
		Password: string(hashedPassword),
	}

	_, err = database.DBEngine.Insert(user)
	if err != nil {
		return err
	}

	return c.JSON(fiber.Map{"user": user})
}
