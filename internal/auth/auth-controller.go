package auth

import (
	"github.com/gofiber/fiber/v2"
)

func RegisterAuthControllers(r fiber.Router) {

	v1 := r.Group("/v1", func(c *fiber.Ctx) error {
		c.Set("Version", "v1")
		return c.Next()
	})

	// Route /api/v1/register
	v1.Post("/register", RegisterHandler)
}
