package models

type AuthCredentials struct {
	Email    string `validate:"required,email,min=6,max=32"`
	Password string `validate:"required,min=3,max=32"`
}
